-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2012 at 02:19 AM
-- Server version: 5.1.58
-- PHP Version: 5.3.6-13ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mysimpleadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `actionId` int(11) NOT NULL AUTO_INCREMENT,
  `actionName` varchar(50) NOT NULL,
  `resourceId` int(11) NOT NULL,
  PRIMARY KEY (`actionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`actionId`, `actionName`, `resourceId`) VALUES
(1, 'add', 1),
(2, 'edit', 1),
(3, 'delete', 1),
(4, 'index', 1),
(5, 'index', 2),
(6, 'add', 3),
(7, 'edit', 3),
(8, 'delete', 3),
(9, 'index', 3),
(10, 'add', 4),
(11, 'edit', 4),
(12, 'delete', 4),
(13, 'index', 4),
(14, 'add', 5),
(15, 'edit', 5),
(16, 'delete', 5),
(17, 'index', 5),
(18, 'change-password', 5),
(19, 'add', 6),
(20, 'edit', 6),
(21, 'delete', 6),
(22, 'index', 6);

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE IF NOT EXISTS `resource` (
  `resourceId` int(11) NOT NULL AUTO_INCREMENT,
  `resourceName` varchar(50) NOT NULL,
  `resourceModule` enum('ADMIN','FRONT','MOBILE') NOT NULL,
  PRIMARY KEY (`resourceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`resourceId`, `resourceName`, `resourceModule`) VALUES
(1, 'action', 'ADMIN'),
(2, 'index', 'ADMIN'),
(3, 'resource', 'ADMIN'),
(4, 'role', 'ADMIN'),
(5, 'user', 'ADMIN'),
(6, 'index', 'FRONT');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(50) NOT NULL,
  `rolePrivileges` text NOT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `roleName` (`roleName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleId`, `roleName`, `rolePrivileges`) VALUES
(1, 'Super Admin', '{"ADMIN":{"action":["add","delete","edit","index"],"index":["index"],"resource":["add","delete","edit","index"],"role":["add","delete","edit","index"],"user":["add","change-password","delete","edit","index"]},"FRONT":{"index":["index"]}}');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userName` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `firstName`, `lastName`, `username`, `password`, `salt`, `email`, `roleId`) VALUES
(1, 'Super', 'Admin', 'admin', '597b397647ee30a06fa968525fe4d7ba211aca51', '202cb962ac59075b964b07152d234b70', 'adham.sa3d@gmail.com', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
