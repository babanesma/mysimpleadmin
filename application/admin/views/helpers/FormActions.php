<?php

class Zend_View_Helper_FormActions extends Zend_View_Helper_Abstract{
    
    public function formActions($controller , $id) {
        $acl = My_Acl::getInstance(My_Acl::ADMIN_MODULE);
        $actions = "";
        if($acl->isAllowed($controller , "edit")){
            $editAction = '<span class="icon">';
            $editAction .= '<a href="';
            $editAction .= $this->view->url(
                    array(
                        "controller" => $controller, 
                        "action" => "edit", 
                        "module" => "admin",
                        "{$controller}Id" => $id
                        ), '', true, true);
            $editAction .= '">';
            $editAction .= '<img src="/images/admin/edit.png" />';
            $editAction .= '</a>';
            $editAction .= '</span>';
            $actions .= $editAction;
        }
        
        if($acl->isAllowed($controller , "delete")){
            $deleteAction = '<span class="icon">';
            $deleteAction .= '<a href="';
            $deleteAction .= $this->view->url(
                    array(
                        "controller" => $controller, 
                        "action" => "delete", 
                        "module" => "admin",
                        "{$controller}Id" => $id
                        ), '', true, true);
            $deleteAction .= '" onclick="return confirm(\'Are you sure you want to delete this item ?\')">';
            $deleteAction .= '<img src="/images/admin/delete.png" />';
            $deleteAction .= '</a>';
            $deleteAction .= '</span>';
            $actions .= $deleteAction;
        }
        return $actions;
    }
}