<?php

class Admin_Form_Action extends My_Form_Admin {
    
    public function init(){
        parent::init();
        
        $actionName = new Zend_Form_Element_Text("actionName");
        $actionName->setLabel("Action Name");
        $actionName->setRequired();
        $actionName->addValidator(new Zend_Validate_StringLength(array("max"=>50)));
        
        $resourceId = new Zend_Form_Element_Select("resourceId");
        $resourceId->setLabel("Resource");
        $resourceModel = new Admin_Model_Resource();
        $resources = $resourceModel->getAllResources();
        foreach($resources as $r){
            $resourceId->addMultiOption( $r->getResourceId() , $r->getResourceModule() . " - " .$r->getResourceName());
        }
        
        $this->addElements(array(
            $actionName,
            $resourceId
        ));
        
        $this->addSaveButton();
    }   
}
