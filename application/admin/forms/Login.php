<?php

class Admin_Form_Login extends My_Form_Admin {
    
    public function init() {
        
        $username = new Zend_Form_Element_Text("username");
        $username->setLabel("username");
        $username->setRequired();
        $username->addValidator(new Zend_Validate_Alnum(true));
        
        $password = new Zend_Form_Element_Password("password");
        $password->setLabel("password");
        $password->setRequired();
        $password->addValidator(new Zend_Validate_Alnum(true));
        
        $login = new Zend_Form_Element_Submit("login");
        $login->setLabel("login");
        
        $this->addElements(array(
            $username,
            $password,
            $login
        ));
    }
}