<?php

class Admin_Form_Resource extends My_Form_Admin {
    
    public function init(){
        parent::init();
        
        $resourceName = new Zend_Form_Element_Text("resourceName");
        $resourceName->setLabel("Resource Name");
        $resourceName->setRequired();
        $resourceName->addFilter(new Zend_Filter_StringTrim());
        $resourceName->addValidator(new Zend_Validate_StringLength(array("max" => 50)));
        
        $resourceModule = new Zend_Form_Element_Select("resourceModule");
        $resourceModule->setLabel("Resource Module");
        $resourceModule->setRequired();
        $resourceModule->addMultiOptions(array(
            "ADMIN" => "ADMIN",
            "FRONT" => "FRONT",
            "MOBILE" => "MOBILE"));
        
        $this->addElements(array(
            $resourceName,
            $resourceModule
        ));
        
        $this->addSaveButton();
    }   
}
