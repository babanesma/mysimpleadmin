<?php

class Admin_Form_User extends My_Form_Admin {
    
    public function init() {
        parent::init();
        
        $username = new Zend_Form_Element_Text("username");
        $username->setLabel("username");
        $username->setRequired();
        $username->addValidator(new Zend_Validate_Alnum());
        $username->addValidator(new Zend_Validate_StringLength(array("max" => 50)));
        $username->addValidator(new Zend_Validate_Db_NoRecordExists(array(
            "table" => "user",
            "field" => "username"
        )));
        
        $firstName = new Zend_Form_Element_Text("firstName");
        $firstName->setLabel("First Name");
        $firstName->setRequired();
        $firstName->addValidator(new Zend_Validate_Alnum(true));
        $firstName->addValidator(new Zend_Validate_StringLength(array("max" => 50)));
        $firstName->addFilter(new Zend_Filter_StringTrim());
        
        $lastName = new Zend_Form_Element_Text("lastName");
        $lastName->setLabel("Last Name");
        $lastName->setRequired();
        $lastName->addValidator(new Zend_Validate_Alnum(true));
        $lastName->addValidator(new Zend_Validate_StringLength(array("max" => 50)));
        $lastName->addFilter(new Zend_Filter_StringTrim());
        
        $password = new Zend_Form_Element_Password("password");
        $password->setLabel("password");
        $password->setRequired();
        
        $confirmPassword = new Zend_Form_Element_Password("confirmPassword");
        $confirmPassword->setLabel("confirm password");
        $confirmPassword->setRequired();
        $confirmPassword->addValidator('Identical', false, array('token' => 'password'));
        $confirmPassword->addErrorMessage('The passwords do not match');
        
        $email = new Zend_Form_Element_Text("email");
        $email->setLabel("email");
        $email->setRequired();
        $email->addValidator(new Zend_Validate_EmailAddress());
        $email->addValidator(new Zend_Validate_StringLength(array("max" => 50)));
        $email->addValidator(new Zend_Validate_Db_NoRecordExists(array(
            "table" => "user",
            "field" => "email"
        )));
        
        $roleId = new Zend_Form_Element_Select("roleId");
        $roleId->setLabel("Role");
        $roleId->setRequired();
        $roleModel = new Admin_Model_Role();
        $roles = $roleModel->getAllRoles();
        foreach($roles as $role){
            $roleId->addMultiOption($role->getRoleId() , $role->getRoleName());
        }
        
        $this->addElements(array(
            $username,
            $firstName,
            $lastName,
            $password,
            $confirmPassword,
            $email,
            $roleId 
        ));
        
        $this->addSaveButton();        
    }
    
}