<?php

class Admin_Form_Role extends My_Form_Admin {
    
    public function init() {
        parent::init();
        
        $roleName = new Zend_Form_Element_Text("roleName");
        $roleName->setLabel("Role Name");
        $roleName->setRequired();
        $roleName->addValidator(new Zend_Validate_Alnum(true));
        $roleName->addValidator(new Zend_Validate_StringLength(array("max" => 50)));
        
        $this->addElement($roleName);
        
        $resourceModel = new Admin_Model_Resource();
        $resourcesWithActions = $resourceModel->getResourcesWithActions();
        
        foreach($resourcesWithActions as $module => $resources){
            $moduleElements = array();
            foreach($resources as $resourceName => $resource){
                $resourceElement = new Zend_Form_Element_MultiCheckbox($module . "_" .$resourceName);
                $resourceElement->setSeparator("")->setBelongsTo("Actions");
                $resourceElement->setLabel($resourceName);
                foreach($resource["actions"] as $action){
                    $resourceElement->addMultiOption($action["actionName"] , $action["actionName"]);
                }
                
                $moduleElements[] = $resourceElement;
            }
            $this->addDisplayGroup($moduleElements, "{$module}_resources" , array("legend" => "$module Resources"));
        }
        $this->addSaveButton();
        
    }
}
