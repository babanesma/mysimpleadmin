<?php

class Admin_Form_ChangePassword extends My_Form_Admin {
    
    public function init() {
        parent::init();
        
        $newPassword = new Zend_Form_Element_Password("newPassword");
        $newPassword->setLabel("New Password");
        $newPassword->setRequired();
        
        $confirmNewPassword = new Zend_Form_Element_Password("confirmNewPassword");
        $confirmNewPassword->setLabel("Confirm New Password");
        $confirmNewPassword->setRequired();
        $confirmNewPassword->addValidator("Identical" , false , array("token" => "newPassword"));
        $confirmNewPassword->addErrorMessage('The passwords do not match');
        
        $this->addElements(array(
           $newPassword , 
           $confirmNewPassword
        ));
        
        $this->addSaveButton();
    }
}