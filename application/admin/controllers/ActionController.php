<?php

class Admin_ActionController extends My_Controller_Action_Admin {
    
    public function indexAction(){
        $resource = $this->getRequest()->getParam("resource");
        $actionModel = new Admin_Model_Action();
        if(isset($resource)) {
            $allActions = $actionModel->getAllActions($resource);
            $this->view->resource = $resource;
        } else {
            $allActions = $actionModel->getAllActions();
        }
        
        $this->view->allActions = $allActions;
    }
    
    public function addAction(){
        $actionForm = new Admin_Form_Action();
        $actionModel = new Admin_Model_Action();
        
        if($this->getRequest()->isPost()){
            $data = $this->getRequest()->getParams();
            if($actionForm->isValid($data)){
                $newAction = new My_Object_Action();
                $newAction->populate($data);
                $actionModel->addAction($newAction);
                $this->_redirect("admin/action/index");
            }
        }
        $this->view->form = $actionForm;
    }
    
    public function editAction(){
        $actionForm = new Admin_Form_Action();
        $actionModel = new Admin_Model_Action();
        $actionId = $this->getRequest()->getParam("actionId");
        
        if($this->getRequest()->isPost()){
            $data = $this->getRequest()->getParams();
            if($actionForm->isValid($data)){
                $editedAction = new My_Object_Action();
                $editedAction->populate($data);
                $actionModel->editAction($actionId , $editedAction);
                $this->_redirect("admin/action/index");
            }
        } else {
            $action = $actionModel->getActionById($actionId);
            $actionForm->populate($action->toArray());
        }
        $this->view->form = $actionForm;
    }
    
    public function deleteAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $actionModel = new Admin_Model_Action();
        $actionId = $this->getRequest()->getParam("actionId");
        
        $actionModel->deleteAction($actionId);
        
        $this->_redirect("admin/action/index");
    }
    
}