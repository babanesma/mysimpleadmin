<?php

class Admin_UserController extends My_Controller_Action_Admin {

    public function indexAction() {
        $userModel = new Admin_Model_User();
        $allUsers = $userModel->getAllUsers();
        $this->view->allUsers = $allUsers;
    }

    public function addAction() {
        $userForm = new Admin_Form_User();
        $userModel = new Admin_Model_User();

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if ($userForm->isValid($data)) {
                $newuser = new My_Object_User();
                $newuser->setUsername($data["username"]);
                $newuser->setFirstName($data["firstName"]);
                $newuser->setLastName($data["lastName"]);
                $newuser->setEmail($data["email"]);
                $salt = md5($data["password"]);
                $newuser->setSalt($salt);
                $password = $userModel->encryptPassword($data["password"] , $salt);
                $newuser->setPassword($password);
                $newuser->setRoleId($this->getRequest()->getParam("roleId"));

                $userModel->addUser($newuser);
                $this->_redirect("admin/user/index");
            }
        }
        $this->view->form = $userForm;
    }

    public function editAction() {
        $userForm = new Admin_Form_User();
        $userForm->removeElement("password");
        $userForm->removeElement("confirmPassword");
        $userModel = new Admin_Model_User();
        $userId = $this->getRequest()->getParam("userId");

        $userForm->getElement("username")->removeValidator("Db_NoRecordExists");
        $userForm->getElement("email")->removeValidator("Db_NoRecordExists");
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if ($userForm->isValid($data)) {
                $userToEdit = new My_Object_User();
                $userToEdit->setUsername($data["username"]);
                $userToEdit->setFirstName($data["firstName"]);
                $userToEdit->setLastName($data["lastName"]);
                $userToEdit->setEmail($data["email"]);
                $userToEdit->setRoleId($data["roleId"]);
                $userModel->editUser($userId, $userToEdit);
                $this->_redirect("admin/user/index");
            }
        } else {
            $user = $userModel->getUserById($userId);
            $userForm->populate($user->toArray());
        }
        $this->view->userId = $userId;
        $this->view->form = $userForm;
    }

    public function deleteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $userModel = new Admin_Model_User();
        $userId = $this->getRequest()->getParam("userId");

        $userModel->deleteUser($userId);

        $this->_redirect("admin/user/index");
    }

    public function changePasswordAction() {
        $changePasswordForm = new Admin_Form_ChangePassword();
        $userModel = new Admin_Model_User();
        $userId = $this->getRequest()->getParam("userId");

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if ($changePasswordForm->isValid($data)) {
                $newPassword = $this->getRequest()->getParam("newPassword");
                $newSalt = md5($newPassword);
                $newPassword = $userModel->encryptPassword($newPassword , $newSalt);
                $userModel->changePassword($userId, $newPassword , $newSalt);
                $this->_redirect("admin/user/index");
            }
        }

        $this->view->form = $changePasswordForm;
    }

}