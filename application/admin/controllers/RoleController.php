<?php

class Admin_RoleController extends My_Controller_Action_Admin {

    public function indexAction() {
        $roleModel = new Admin_Model_Role();
        $allRoles = $roleModel->getAllRoles();
        $this->view->allRoles = $allRoles;
    }

    public function addAction() {
        $roleForm = new Admin_Form_Role();
        $roleModel = new Admin_Model_Role();

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if ($roleForm->isValid($data)) {
                $roleData = array(
                    "roleName" => $roleForm->getValue("roleName")
                );
                $roleActions = array();
                $actionsData = $data["Actions"];
                foreach($actionsData as $key => $value){                    
                    list($module , $resource) = explode("_" , $key);
                    $roleActions[$module][$resource] = $value;
                }
                $roleData["rolePrivileges"] = Zend_Json::encode($roleActions);
                $roleModel->addRole($roleData);

                $this->_redirect("admin/role/index");
            }
        }
        $this->view->form = $roleForm;
    }

    public function editAction() {
        $roleForm = new Admin_Form_Role();
        $roleModel = new Admin_Model_Role();
        $roleId = $this->getRequest()->getParam("roleId");

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParams();
            if ($roleForm->isValid($data)) {
                $roleData = array(
                    "roleName" => $roleForm->getValue("roleName")
                );

                $roleActions = array();
                $actionsData = $data["Actions"];
                foreach($actionsData as $key => $value){                    
                    list($module , $resource) = explode("_" , $key);
                    $roleActions[$module][$resource] = $value;
                }

                $roleData["rolePrivileges"] = Zend_Json::encode($roleActions);
                $roleModel->editRole($roleId, $roleData);
                $this->_redirect("admin/role/index");
            }
        } else {
            $roleToEdit = $roleModel->getRoleById($roleId);
            $roleForm->getElement("roleName")->setValue($roleToEdit->getRoleName());

            $rolePrivilegesData = Zend_Json::decode($roleToEdit->getRolePrivileges());
            foreach ($rolePrivilegesData as $module => $resources) {
                foreach($resources as $resource => $actions){
                    $roleForm->getElement($module . "_" . $resource)->setValue($actions);
                }
            }
        }
        $this->view->form = $roleForm;
    }

    public function deleteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $roleModel = new Admin_Model_Role();
        $roleId = $this->getRequest()->getParam("roleId");

        $roleModel->deleteRole($roleId);

        $this->_redirect("admin/role/index");
    }

}