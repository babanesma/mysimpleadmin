<?php

class Admin_IndexController extends My_Controller_Action_Admin{
    
    public function indexAction(){
        $this->view->headScript()->offsetSetFile(100 , "/js/admin/homepage.js");
        $message = $this->getRequest()->getParam("message");
        $this->view->message = ($message !== null) ? $message : 0;
    }
}