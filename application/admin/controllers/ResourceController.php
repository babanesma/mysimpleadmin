<?php

class Admin_ResourceController extends My_Controller_Action_Admin {
    
    public function indexAction(){
        $resourceModel = new Admin_Model_Resource();
        $allResources = $resourceModel->getAllResources();
        $this->view->allResources = $allResources;
    }
    
    public function addAction(){
        $resourceForm = new Admin_Form_Resource();
        $resourceModel = new Admin_Model_Resource();
        $data = $this->getRequest()->getParams();
        
        if($this->getRequest()->isPost()){
            if($resourceForm->isValid($data)){
                $newResource = new My_Object_Resource();
                $newResource->populate($data);
                $resourceModel->addResource($newResource);
                $this->_redirect("admin/resource/index");
            }
        }
        $this->view->form = $resourceForm;
    }
    
    public function editAction(){
        $resourceForm = new Admin_Form_Resource();
        $resourceModel = new Admin_Model_Resource();
        $resourceId = $this->getRequest()->getParam("resourceId");
        
        if($this->getRequest()->isPost()){
            $data = $this->getRequest()->getParams();
            if($resourceForm->isValid($data)){
                $editedResource = new My_Object_Resource();
                $editedResource->populate($data);
                $resourceModel->editRole($resourceId , $editedResource);
                $this->_redirect("admin/resource/index");
            }
        } else {
            $resource = $resourceModel->getResourceById($resourceId);
            $resourceForm->populate($resource->toArray());
        }
        $this->view->form = $resourceForm;
    }
    
    public function deleteAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $resourceForm = new Admin_Form_Resource();
        $resourceModel = new Admin_Model_Resource();
        $resourceId = $this->getRequest()->getParam("resourceId");
        
        $resourceModel->deleteResource($resourceId);
        $this->_redirect("admin/resource/index");
    }
}