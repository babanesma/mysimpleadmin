<?php

class Admin_SignController extends Zend_Controller_Action {

    public function inAction() {
        $loginForm = new Admin_Form_Login();

        if ($this->getRequest()->isPost()) {
            $auth = My_Auth::getInstance();

            $username = $this->getRequest()->getParam("username");
            $password = $this->getRequest()->getParam("password");

            $authResult = $auth->login($username, $password);
            if ($authResult["result"]) {
                $this->_redirect("/admin/index/index/message/{$authResult["message"]}");
            } else {
                $this->view->message = $authResult["message"];
            }
        }
        $this->view->loginForm = $loginForm;
    }

    public function outAction() {
        
        $auth = My_Auth::getInstance();
        $auth->logout();
        $this->_redirect("/admin/index/index");
    }

}