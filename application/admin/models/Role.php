<?php

class Admin_Model_Role extends My_Model_Admin {

    public function __construct() {
        $this->dbTable = new My_DbTable_Role();
    }

    public function getAllRoles() {
        $rows = $this->dbTable->fetchAll();
        $roles = array();
        foreach ($rows as $row) {
            $role = new My_Object_Role();
            $role->populate($row->toArray());
            $roles[] = $role;
        }
        return $roles;
    }

    public function addRole($role) {
        if (!is_array($role) && is_a("My_Object_Role", $role)) {
            $role = $role->toArray();
        }
        return $this->dbTable->insert($role);
    }

    public function deleteRole($roleId) {
        $this->dbTable->delete("roleId = $roleId");
    }

    public function getRoleById($roleId) {
        $row = $this->dbTable->fetchRow("roleId = $roleId");
        $role = new My_Object_Role();
        $role->populate($row->toArray());
        return $role;
    }
    
    public function getRoleByName($roleName){
    	$row = $this->dbTable->fetchRow("roleName='$roleName'");
    	$role = new My_Object_Role();
    	$role->populate($row->toArray());
    	return $role;
    }

    public function editRole($roleId, $roleData) {
        $this->dbTable->update($roleData, "roleId = $roleId");
    }

}
