<?php

class Admin_Model_Action extends My_Model_Admin {
    
    public function __construct() {
        $this->dbTable = new My_DbTable_Action();
    }
    
    public function getAllActions($resource = false){
        $select = $this->dbTable->select()->setIntegrityCheck(false);
        $select->from("action" , array("actionId" , "actionName" ));
        $select->join("resource", "resource.resourceId = action.resourceId" , array("resourceName"));
        if($resource){
            $select->where("resource.resourceName = ?" , $resource);
        }
        $select->order(array("resourceName" , "actionName"));
        
        $rows = $select->query()->fetchAll();
        return $rows;
    }
        
    public function addAction(My_Object_Action $action){ 
        $this->dbTable->insert($action->toArray());
    }
    
    public function getActionById($actionId){
        $action = new My_Object_Action();
        $row = $this->dbTable->fetchRow("actionId = $actionId");
        $action->populate($row->toArray());
        return $action;
    }
    
    public function editAction($actionId , My_Object_Action $action){
        $data = $action->toArray();
        $this->dbTable->update($data, "actionId = $actionId");
    }
    
    public function deleteAction($actionId){
        $this->dbTable->delete("actionId = $actionId");
    }
}