<?php

class Admin_Model_User extends My_Model_Admin {

    public function __construct() {
        $this->dbTable = new My_DbTable_User();
    }
    
    public function getAllUsers(){
        $select = $this->dbTable->select()->setIntegrityCheck(false);
        $select->from("user" , array("userId" , "username" , "firstName" , "lastName"));
        $select->join("role", "user.roleId = role.roleId" , "roleName");
        
        return $select->query()->fetchAll();
    }
    
    public function addUser(My_Object_User $user){
        return $this->dbTable->insert($user->toArray());
    }
    
    public function editUser($userId , My_Object_User $user){
        $userData = $user->toArray();
        return $this->dbTable->update($user->toArray(), "userId = $userId");
    }
    
    public function getUserById($userId){
        $row = $this->dbTable->fetchRow("userId = $userId");
        $user = new My_Object_User();
        $user->populate($row->toArray());
        
        return $user;
    }
    
    public function changePassword($userId , $newPassword , $newSalt){
        $this->dbTable->update(array("password" => $newPassword), "userId = $userId");
        $this->dbTable->update(array("salt" => $newSalt), "userId = $userId");
    }
    
    public function encryptPassword($password , $salt){
        return sha1($password . $salt);
    }
    
    public function deleteUser($userId){
        return $this->dbTable->delete("userId = $userId");
    }
    
}