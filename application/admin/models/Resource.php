<?php

class Admin_Model_Resource extends My_Model_Admin {

    public function __construct() {
        $this->dbTable = new My_DbTable_Resource();
    }

    public function getAllResources($resourceModule = false) {
        if ($resourceModule) {
            $rows = $this->dbTable->fetchAll("resourceModule = '$resourceModule'", array("resourceModule", "resourceName"));
        } else {
            $rows = $this->dbTable->fetchAll(null, array("resourceModule", "resourceName"));
        }
        $resources = array();
        foreach ($rows as $r) {
            $resource = new My_Object_Resource();
            $resource->populate($r->toArray());
            $resources[] = $resource;
        }
        return $resources;
    }

    public function getResourcesWithActions() {
        $select = $this->dbTable->select()->setIntegrityCheck(false);
        $select->from("resource", array("resourceId", "resourceName", "resourceModule"));
        $select->join("action", "resource.resourceId = action.resourceId", array("actionId", "actionName"));
        $select->order(array("resourceModule", "resourceName", "actionName"));

        $rows = $select->query()->fetchAll();
        $resourcesWithActions = array();
        foreach ($rows as $row) {
            $resourcesWithActions[$row["resourceModule"]][$row["resourceName"]]["actions"][] = array(
                "actionId" => $row["actionId"],
                "actionName" => $row["actionName"]
            );
            $resourcesWithActions[$row["resourceModule"]][$row["resourceName"]]["resourceName"] = $row["resourceName"];
            $resourcesWithActions[$row["resourceModule"]][$row["resourceName"]]["resourceId"] = $row["resourceId"];
        }
        return $resourcesWithActions;
    }

    public function addResource(My_Object_Resource $resource) {
        $this->dbTable->insert($resource->toArray());
    }

    public function getResourceById($resourceId) {
        $row = $this->dbTable->fetchRow("resourceId = $resourceId");
        $resource = new My_Object_Resource();
        $resource->populate($row->toArray());

        return $resource;
    }

    public function editRole($resourceId, My_Object_Resource $resource) {
        $data = $resource->toArray();
        $this->dbTable->update($data, "resourceId = $resourceId");
    }

    public function deleteResource($resourceId) {
        $this->dbTable->delete("resourceId = $resourceId");
    }

}