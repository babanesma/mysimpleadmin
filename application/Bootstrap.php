<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    public function _initAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(
                        array('namespace' => 'Default',
                            'basePath' => dirname(__FILE__) . '/default',
                        )
        );
        
        $autoloader2 = new Zend_Application_Module_Autoloader(
                        array('namespace' => 'Admin',
                            'basePath' => dirname(__FILE__) . '/admin',
                        )
        );
        
        Zend_Session::start();
    }

    protected function _initLayoutHelper() {
        $this->bootstrap('frontController');
        $layout = Zend_Controller_Action_HelperBroker::addHelper(
                        new My_Controller_Action_Helper_LayoutLoader());
    }
    
    protected function _initCustomErrorHandler(){
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new My_Controller_Plugin_CustomErrorHandler());
    }
    
    protected function _initMobileDetect(){
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new My_Controller_Plugin_MobileDetect());
    }
}
