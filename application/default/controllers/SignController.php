<?php

class SignController extends Zend_Controller_Action {

    public function indexAction(){
        $auth = My_Auth::getInstance();
        if($auth->isLogged()){
            $this->_redirect("/index/index/");
        } else {
            $this->_redirect("/sign/in/");
        }
    }
    public function inAction() {
        $loginForm = new Default_Form_Login();

        if ($this->getRequest()->isPost()) {
            $auth = My_Auth::getInstance();

            $username = $this->getRequest()->getParam("username");
            $password = $this->getRequest()->getParam("password");

            $authResult = $auth->login($username, $password);
            if ($authResult["result"]) {
                $this->_redirect("/index/index/message/{$authResult["message"]}");
            } else {
                $this->view->message = $authResult["message"];
            }
        }
        $this->view->loginForm = $loginForm;
    }

    public function outAction() {
        
        $auth = My_Auth::getInstance();
        $auth->logout();
        $this->_redirect("/index/index");
    }

}