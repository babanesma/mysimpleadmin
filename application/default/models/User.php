<?php

class Default_Model_User extends My_Model_Front {

    public function __construct() {
        $this->dbTable = new My_DbTable_User();
    }
    
    public function registerNewUser(My_Object_User $user){
        return $this->dbTable->insert($user->toArray());
    }
    
    public function getUserById($userId){
        $row = $this->dbTable->fetchRow("userId = $userId");
        $user = new My_Object_User();
        $user->populate($row->toArray());
        
        return $user;
    }
    
    public function changePassword($userId , $newPassword , $newSalt){
        $this->dbTable->update(array("password" => $newPassword), "userId = $userId");
        $this->dbTable->update(array("salt" => $newSalt), "userId = $userId");
    }
    
    public function encryptPassword($password , $salt){
        return sha1($password . $salt);
    }
    
    
}