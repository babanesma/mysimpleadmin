<?php

class My_Object_User {

    private $userId;
    private $firstName;
    private $lastName;
    private $username;
    private $password;
    private $salt;
    private $email;
    private $roleId;

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPassword() {
        return $this->password;
    }
    
    public function setSalt($salt) {
        $this->salt = $salt;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setRoleId($roleId) {
        $this->roleId = $roleId;
    }

    public function getRoleId() {
        return $this->roleId;
    }

    public function populate($data) {
        if (isset($data["userId"]))
            $this->setUserId($data["userId"]);
        if (isset($data["firstName"]))
            $this->setFirstName($data["firstName"]);
        if (isset($data["lastName"]))
            $this->setLastName($data["lastName"]);
        if (isset($data["username"]))
            $this->setUsername($data["username"]);
        if (isset($data["password"]))
            $this->setPassword($data["password"]);
        if (isset($data["salt"]))
            $this->setSalt($data["salt"]);
        if (isset($data["email"]))
            $this->setEmail($data["email"]);
        if (isset($data["roleId"]))
            $this->setRoleId($data["roleId"]);
        return $this;
    }

    public function toArray() {
        $data = array();
        if (isset($this->userId))
            $data ["userId"] = $this->getUserId();
        if (isset($this->firstName))
            $data ["firstName"] = $this->getFirstName();
        if (isset($this->lastName))
            $data ["lastName"] = $this->getLastName();
        if (isset($this->username))
            $data ["username"] = $this->getUsername();
        if (isset($this->password))
            $data ["password"] = $this->getPassword();
        if (isset($this->salt))
            $data ["salt"] = $this->getSalt();
        if (isset($this->email))
            $data ["email"] = $this->getEmail();
        if (isset($this->roleId))
            $data ["roleId"] = $this->getRoleId();
        return $data;
    }

}

?>